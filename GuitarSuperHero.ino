#include <Bounce2.h>
#include <EEPROM.h>

const uint8_t BUTTON_NUMBER       = 5;  // Algunos controladores tienen seis botones (o más)
const uint8_t NUM_PRESETS         = 3;
const uint8_t NUM_ANALOG_CONTROLS = 2;
uint8_t preset = 0;
uint8_t canal_midi = 1; 
const uint8_t DEBOUNCE_TIME = 5 ;   // milisegundos

const uint8_t NUM_PROGRAMAS = 4;

const boolean DEPURACION = true; // Usar o no usar mensajes de texto por el puerto seriak
const boolean USAR_LEDS  = true; // Usar leds
const boolean USAR_ACEL  = true; // Usar acelerometro
const uint8_t NUM_LEDS = 4;

#define PIN_BOTON_1         17
#define PIN_BOTON_2         16
#define PIN_BOTON_3         15
#define PIN_BOTON_4         14
#define PIN_BOTON_5         13
#define PIN_BOTON_ARR       11
#define PIN_BOTON_ABJ       12
#define PIN_LED_1           7
#define PIN_LED_2           8
#define PIN_LED_3           9
#define PIN_LED_4           10
#define PIN_BOTON_SEL       5
#define PIN_BOTON_START     6

#define PIN_ANALOG_CONTROL_BARRA   A12
#define PIN_ANALOG_PIEZO           A1

const uint8_t pinAnalog[NUM_ANALOG_CONTROLS] = {PIN_ANALOG_CONTROL_BARRA, PIN_ANALOG_PIEZO};
const uint8_t pinLeds[NUM_LEDS] = {PIN_LED_1, PIN_LED_2, PIN_LED_3, PIN_LED_4};

#define NOTA_1          64  // 
#define NOTA_2          67  // 
#define NOTA_3          69  // LA
#define NOTA_4          71  // SI
#define NOTA_5          74  // RE
#define NOTA_AIRE       52  // 

#define LED_POWER       0
#define LED_NOTE_ON     4


boolean estadoLeds[NUM_LEDS] = {false,false,false,false};
uint8_t modo = 0;
uint8_t programa = 0;
uint8_t banco = 0;;


const int ANALOG_DB_BARRA = 5;
const int ANALOG_DB_PIEZO = 5;
const uint8_t analogDeadband[NUM_ANALOG_CONTROLS] = {ANALOG_DB_BARRA,ANALOG_DB_PIEZO };


const uint8_t pinBoton[BUTTON_NUMBER] = {PIN_BOTON_1,PIN_BOTON_2,PIN_BOTON_3,PIN_BOTON_4,PIN_BOTON_5};
const uint8_t notas[NUM_PRESETS][BUTTON_NUMBER] = {
  {NOTA_1,NOTA_2,NOTA_3,NOTA_4,NOTA_5},
  {NOTA_1,NOTA_2,NOTA_3,NOTA_4,NOTA_5},
  {NOTA_1,NOTA_2,NOTA_3,NOTA_4,NOTA_5}
};

boolean notasEstado[BUTTON_NUMBER] = {};
boolean botonesSueltos=true;
boolean ledsEstado[NUM_LEDS] = {}; 

unsigned int analog_barra_viejo = 0 ;
int pitchbend = 0;



Bounce boton[BUTTON_NUMBER]  = Bounce();
Bounce botonArriba = Bounce();
Bounce botonAbajo = Bounce();
Bounce botonSelect = Bounce();
Bounce botonStart  = Bounce();

void setup() {
  if (DEPURACION) {
    Serial.begin(9600);
  }
  for (uint8_t i = 0; i< BUTTON_NUMBER; i++) {
    pinMode(pinBoton[i],INPUT_PULLUP);
    boton[i].attach(pinBoton[i]);
    boton[i].interval(DEBOUNCE_TIME);
    notasEstado[i] = false;
  }

  for (uint8_t i = 0; i< NUM_LEDS; i++) {
    pinMode(pinLeds[i],OUTPUT);
    estadoLeds[LED_POWER] = true;
  }


  pinMode(PIN_BOTON_ARR,INPUT_PULLUP);
  pinMode(PIN_BOTON_ABJ,INPUT_PULLUP);
  pinMode(PIN_BOTON_SEL,INPUT_PULLUP);
  pinMode(PIN_BOTON_START,INPUT_PULLUP);
  
  botonArriba.attach(PIN_BOTON_ARR);
  botonArriba.interval(DEBOUNCE_TIME);
  
  botonAbajo.attach(PIN_BOTON_ABJ);
  botonAbajo.interval(DEBOUNCE_TIME);
  
  botonSelect.attach(PIN_BOTON_SEL);
  botonSelect.interval(DEBOUNCE_TIME);
  
  botonStart.attach(PIN_BOTON_START);
  botonStart.interval(DEBOUNCE_TIME); 

  if (DEPURACION) {
    Serial.println("It's show time!");
  }
}
void loop() {
  while (usbMIDI.read()) {
    // Descartamos todos los mensajes entrantes
  }
  gestionarBotonesNotas();
  gestionarBarra();
  gestionarBotonesModos();
  gestionarLeds();
  delay(10);
}


void gestionarBotonesNotas() {
  for (uint8_t i = 0; i < BUTTON_NUMBER; i++) {
    boton[i].update();
    
    if (boton[i].read()  == HIGH) {
     
      if (notasEstado[i]== true) {
        usbMIDI.sendNoteOff(notas[preset][i],0,canal_midi);
        notasEstado[i] = false;
        if (DEPURACION) {
          Serial.print("Note "); Serial.print(notas[preset][i]);Serial.println(" off");
        }
      }
    }
  }
  botonAbajo.update();
  botonArriba.update();
  botonesSueltos=true;
  if (botonAbajo.fell() || botonArriba.fell() ) {  // Let's Rock!   
    for (uint8_t i = 0; i < BUTTON_NUMBER; i++) {
      if (boton[i].read() == LOW) {
        botonesSueltos=false;
        if (notasEstado[i] == true ) {
          usbMIDI.sendNoteOff(notas[preset][i],0,canal_midi);
          usbMIDI.sendNoteOn(notas[preset][i],127,canal_midi);
          
        }
        usbMIDI.sendNoteOn(notas[preset][i],127,canal_midi); 
        notasEstado[i] = true;     
      }
    }
/*    if (botonesSueltos=true) {
      usbMIDI.sendNoteOff((notas[preset][0])-12,0,canal_midi);
      usbMIDI.sendNoteOn((notas[preset][0])-12,127,canal_midi);
      Serial.println("Botones sueltos, lanzamos nota suelta");
    } else {
        usbMIDI.sendNoteOff((notas[preset][0])-12,0,canal_midi);
        Serial.println("Boton pulsado, paramos nota suelta");
    }*/
  } 
}


void gestionarBarra() {
  unsigned int valor_barra = analogRead(PIN_ANALOG_CONTROL_BARRA); {
      if ((valor_barra < analog_barra_viejo - ANALOG_DB_BARRA) || (valor_barra > analog_barra_viejo + ANALOG_DB_BARRA)) {
        analog_barra_viejo = valor_barra;
        unsigned int pitchbend = map(valor_barra,1023,0,0,-16383);
          usbMIDI.sendPitchBend(pitchbend,canal_midi);
        if (DEPURACION) {
            Serial.print("Pitchbend "); Serial.println(pitchbend);
          }
      }
    }
}

void gestionarBotonesModos() {
  botonStart.update();
  botonSelect.update();
  if (botonSelect.rose() ) {
    if (modo < NUM_PROGRAMAS -1) {
      modo++;
    } else {
      modo = 0;
    }
    selectPrograma(programa);
    usbMIDI.sendControlChange(0,0,canal_midi);
    usbMIDI.sendControlChange(32,banco,canal_midi);
    usbMIDI.sendProgramChange(programa,canal_midi);
    if (DEPURACION) {
       Serial.print("Cambio de modo "); Serial.print(banco); Serial.print(",  "); Serial.println(programa);
    }
  }
}


void gestionarLeds() {
  for (uint8_t led = 0; led < NUM_LEDS ; led ++) {
    if (estadoLeds[led]== true) {
      digitalWrite(pinLeds[led],LOW);
    } else {
      digitalWrite(pinLeds[led],HIGH);
    }
  }
}


void selectPrograma(uint8_t p) {
  switch (p) {
    case 0:     // Guitarra eléctrica
      banco = 40;
      programa = 3;
      break;
    case 1:     // Guitarra
      banco = 40;
      programa = 65;
      break;
   case 2:     // Bajo
      banco = 10;
      programa = 6;
      break;
   case 3:     // Flauta
      banco = 70;
      programa = 2;
      break;
    default:
      banco = 40;
      programa = 3;
      break;
  }
}

